"use strict";

import OrderForm from './OrderForm.js';

$(function () {
    $('form.sell-conveniently__make-order').on("submit", function (event) {
        let _form = $(this),
            _name = _form.find("input[name=name]").val(),
            _phone = _form.find("input[name=phone]").val(),
            orderForm = new OrderForm(_name, _phone),

            isValidated = orderForm.validate();

        if (isValidated) {
            alert('Форма заполнена успешно. Отправляем данные...\n' +
                  'На сервер уйдут следующие данные:\n' +
                  `Имя: ${orderForm.name}\n` +
                  `Телефон: ${orderForm.phone}`);
        } else {
            alert(orderForm.getErrors());
        }

        event.stopPropagation();
        return false;
    });
});