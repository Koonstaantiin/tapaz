"use strict";

export default class OrderForm {
    constructor(name = null, phone = null) {
        this._name = name;
        this._phone = Number(phone);
        this._errors = [];

        this.NAME_MINIMUM_LENGTH = 3;
        this.PHONE_MINIMUM_LENGTH = 5;
    }

    get name() {
        return this._name;
    }

    set name(newName) {
        this._name = newName;
    }

    get phone() {
        return this._phone;
    }

    set phone(newPhone) {
        this._phone = Number(newPhone);
    }

    addError(error) {
        this._errors.push(error);
    }

    clearErrors() {
        this._errors = [];
    }

    /**
     * Пример простой валидации
     * @returns {boolean}
     */
    validate() {
        let isName = (typeof this._name === "string" && this._name.length >= this.NAME_MINIMUM_LENGTH),
            isPhone = (typeof this._phone === "number" && (this._phone).toString().length >= this.PHONE_MINIMUM_LENGTH);

        this.clearErrors();

        if (!isName || !isPhone) {
            this.addError('В форме допущены ошибки. Проверьте данные.');
        }

        if (!isName) {
            this.addError(`Имя должно содержать от ${this.NAME_MINIMUM_LENGTH} символа(ов)`)
        }

        if (!isPhone) {
            this.addError(`Телефон должен состоять из цифр и иметь длину от ${this.PHONE_MINIMUM_LENGTH} символа(ов)`)
        }

        console.log(typeof this._name, this._name.length);
        console.log(typeof this._phone, (this._phone).toString().length);

        return (isName && isPhone);
    }

    getErrors() {
        return this._errors.join("\n");
    }
}
