const gulp = require("gulp");
const babel = require("gulp-babel");
const minify = require("gulp-minify");
const rename = require("gulp-rename");
const cleanCSS = require("gulp-clean-css");
const sass = require("gulp-sass");
const sourcemaps = require("gulp-sourcemaps");

gulp.task("js-compile", function () {
    return gulp.src("dev/js/**/*.js")
        .pipe(babel())
        .pipe(sourcemaps.init())
        .pipe(minify({
            ext: {
                src: ".js",
                min: ".min.js"
            }
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest("assets/js/"));
});

gulp.task("sass", function () {
    return gulp.src("dev/scss/**/*.scss")
        .pipe(sourcemaps.init())
        .pipe(sass().on("error", sass.logError))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest("assets/css"))
        .pipe(cleanCSS())
        .pipe(rename(
            function (path) {
                path.basename += ".min";
            }
        ))
        .pipe(gulp.dest("assets/css"));
});