# Changelog
All notable changes to this project will be documented in this file.

## [0.0.1] - 2018-07-04
### Added
- Initial commit
- Normalize.css
- Fonts added
- Сверстаны блоки: Header, "Продавайте через магазины Tap.az", "Tap.az для малого бизнеса", 
"Все товары на фирменной странице магазина"

## [0.0.2] - 2018-07-05
### Added
- Сверстаны блоки: "C магазином на Tap.az продавать удобно!", футер
- Добавлена валидация формы подачи заявки

### Changed
- Обновлена структура